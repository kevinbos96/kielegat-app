class Jukebox {
  final String documentId;
  String title, artiest, url;

  Jukebox.fromMap(Map snapshot, String id)
      : documentId = id ?? '',
        title = snapshot['title'] ?? '',
        artiest = snapshot['artiest'] ?? '',
        url = snapshot['url'] ?? '';
}
