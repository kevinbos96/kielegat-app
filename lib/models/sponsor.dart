class Sponsor{ 
  final String name;
  final String website;

  Sponsor(this.name, this.website);


  Sponsor.fromMap(Map snapshot)
      : name = snapshot['name'] ?? '',
        website = snapshot['website'] ?? '';     


}