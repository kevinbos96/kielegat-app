import 'package:flutter/widgets.dart';

class Article with  ChangeNotifier{

  final String documentId;
  final int wpId;
  final String title;
  final String description;
  final String content;
  final String imageUrl;
  final String thumbnail;
  final DateTime datePublished;


   Article.fromMap(Map snapshot,String id) :
        documentId = id ?? '',
        wpId = snapshot['wpId'] ?? 0,
        title = snapshot['title'] ?? '',
        description = snapshot['description'] ?? '',
        content = snapshot['content'] ?? '',
        imageUrl = snapshot['imageUrl'] ?? '',
        thumbnail = snapshot['thumbnail'] ?? '',
        datePublished = DateTime.parse(snapshot['date'])
        ;


}