import 'package:flutter/widgets.dart';

class CustomPage with ChangeNotifier {
  final String documentId;
  final String content;
  final String imageUrl;
  final String mapImageUrl;
  final String title;
  final bool hasLocations;

  final String tel;
  final String url;
  final bool rotating;

  CustomPage.fromMap(Map snapshot, String id)
      : documentId = id ?? '',
        title = snapshot['title'] ?? '',
        content = snapshot['content'] ?? '',
        imageUrl = snapshot['image'] ?? '',
        mapImageUrl = snapshot['mapImageUrl'] ?? '',
        hasLocations = snapshot['hasLocations'] ?? false,
        tel = snapshot['tel'] ?? '',
        url = snapshot['url'] ?? '',
        rotating = snapshot['rotating'] ?? false;
}
