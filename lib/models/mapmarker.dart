import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapMarker {
  final GeoPoint position;
  final String title;
  final String marker_url;
  BitmapDescriptor icon;

  MapMarker(this.title, this.position, this.marker_url);

  Marker toMarker() => Marker(
        markerId: MarkerId(title),
        position: LatLng(
          position.latitude,
          position.longitude,
        ),
        infoWindow: InfoWindow(
          title: title,
        ),
        icon: icon,
      );

  MapMarker.fromMap(Map snapshot)
      : title = snapshot['title'] ?? '',
        position = snapshot['Location'] ?? snapshot['location'],
        marker_url = snapshot['marker_url'] ??
            'https://firebasestorage.googleapis.com/v0/b/kielegat-d87c5.appspot.com/o/FCMImages%2Fmarker_locatie.png?alt=media&token=7af62a45-ee3f-4181-bfa9-51e9c715cab9';
}
