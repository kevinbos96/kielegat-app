import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/widgets.dart';

class EventItem with ChangeNotifier {
  final String documentId;
  final String title;
  final String description;
  final String content;
  final String imageUrl;
  final String thumbnail;
  final String location;
  final DateTime datePublished;
  final DateTime eventDate;
  final GeoPoint eventLocation;
  final String price;
  final String ticketUrl;

  EventItem.fromMap(Map snapshot, String id)
      : documentId = id ?? '',
        title = snapshot['title'] ?? '',
        description = snapshot['description'] ?? '',
        content = snapshot['content'] ?? '',
        imageUrl = snapshot['imageUrl'] ?? '',
        thumbnail = snapshot['thumbnail'] ?? '',
        location = snapshot['location'] ?? 'Centrum Breda',
        datePublished = DateTime.now(),
        eventDate = (snapshot['eventDate'] as Timestamp).toDate(),
        eventLocation = snapshot['eventLocation'] ?? GeoPoint(1,1), //GeoPoint(51.5885816,4.7760554)
        price = snapshot['price'] ?? 'TOEGANG GRATIS',
        ticketUrl = snapshot['ticketUrl'] ?? '';
}
