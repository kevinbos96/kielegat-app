import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:kielegat_app/common/maps/maps_helper.dart';
import 'package:kielegat_app/models/mapmarker.dart';

class MapItem {
  final String id;
  final String name;
  final String page_id;
  final int type;
  final List<GeoPoint> polylines;
  final List<MapMarker> markers;
  final GeoPoint centerPoint;
  final double initialZoom;

  final Set<Marker> _markers = Set();

  MapItem(this.id, this.name, this.page_id, this.type, this.polylines,
      this.markers, this.centerPoint, this.initialZoom);

  Polyline toPolyline() => Polyline(
        polylineId: PolylineId(id),
        color: Colors.blue,
        points: toPoints(),
      );

  List<LatLng> toPoints() {
    List<LatLng> points = List<LatLng>();
    polylines.forEach((l) {
      points.add(LatLng(l.latitude, l.longitude));
    });
    return points;
  }

  Future<Set<Marker>> toMarkers() async {
    for (MapMarker markerLocation in markers) {
      final BitmapDescriptor markerImage =
          await MapHelper.getMarkerImageFromUrl(markerLocation.marker_url);
      markerLocation.icon = markerImage;
      _markers.add(markerLocation.toMarker());
    }
    return _markers;
  }

  MapItem.fromMap(Map snapshot, String id)
      : id = id ?? '',
        name = snapshot['name'] ?? '',
        page_id = snapshot['page_id'] ?? '',
        type = snapshot['type'] ?? 0,
        polylines = snapshot['polylines'].cast<GeoPoint>(),
        markers = (snapshot['markers'] as List)
            .map((m) => MapMarker.fromMap(m))
            .toList(),
        centerPoint =
            snapshot['centerPoint'] ?? GeoPoint(51.5865421, 4.77459959999999),
        initialZoom = double.parse(snapshot['initialZoom']) ?? 15;
}
