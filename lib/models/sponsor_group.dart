import 'package:flutter/material.dart';
import 'package:kielegat_app/models/sponsor.dart';

class SponsorGroup with ChangeNotifier {
  final String documentId;
  final String name;
  final String type;
  final List<Sponsor> sponsorList;

  SponsorGroup.fromMap(Map snapshot, String id)
      : documentId = id ?? '',
        name = snapshot['name'] ?? '',
        type = snapshot['type'] ?? '',
        sponsorList = (snapshot['sponsors'] as List).map((s) => Sponsor.fromMap(s)).toList() ?? List<Sponsor>();

}

