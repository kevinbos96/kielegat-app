import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:kielegat_app/pages/events/event_detail_page.dart';
import 'package:kielegat_app/pages/events/event_overview_page.dart';
import 'package:kielegat_app/pages/general_info_page.dart';
import 'package:kielegat_app/pages/home_page.dart';
import 'package:kielegat_app/pages/jukebox_page.dart';
import 'package:kielegat_app/pages/maps_page.dart';
import 'package:kielegat_app/pages/news/news_detail_page.dart';
import 'package:kielegat_app/pages/news/news_overview_page.dart';
import 'package:kielegat_app/pages/sponsors.dart';
import 'package:kielegat_app/routing/route_names.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  var name = settings.name;
  switch (name) {
    case HomeRoute:
      return _getPageRoute(name, HomePage());
    case NewsRoute:
      return _getPageRoute(name, NewsOverview());
    case EventsRoute:
      return _getPageRoute(name, EventsOverview());
    case NewsDetailRoute:
      return _getPageRoute(name, NewsDetailPage(settings.arguments));
    case EventDetailRoute:
      return _getPageRoute(name, EventDetailPage(settings.arguments));
    case LeutpenningenRoute:
      return _getPageRoute(name, GeneralInfoPage(pageId: LeutpenningenRoute));
    case OptochtRoute:
      return _getPageRoute(name, GeneralInfoPage(pageId: OptochtRoute));
    case BrakkenSliertRoute:
      return _getPageRoute(name, GeneralInfoPage(pageId: BrakkenSliertRoute));
    case IntochtPrinsRoute:
      return _getPageRoute(name, GeneralInfoPage(pageId: IntochtPrinsRoute));
    case PrinsRoute:
      return _getPageRoute(name, GeneralInfoPage(pageId: PrinsRoute));
    case Wandeling:
      return _getPageRoute(name, GeneralInfoPage(pageId: Wandeling));
    case GoogleMapsRoute:
      return _getPageRoute(name, MapsPage(pageId: settings.arguments));
    case SponsorRoute:
      return _getPageRoute(name, SponsorPage());
    case JukeBoxRoute:
      return _getPageRoute(name, JukeBoxPage());
    default:
      return _getPageRoute(name, HomePage());
  }
}

FadeRoute _getPageRoute(String routeName, Widget child) {
  return FadeRoute(
    routeName,
    page: child,
  );
}

class FadeRoute extends MaterialPageRoute {
  final String _routeName;
  String get routeName => _routeName;

  final Widget page;
  FadeRoute(this._routeName, {this.page}) : super(builder: (_) => page);
}
