import 'package:get_it/get_it.dart';
import 'package:kielegat_app/providers/event_provider.dart';
import 'package:kielegat_app/providers/general_info_provider.dart';
import 'package:kielegat_app/providers/jukebox_provider.dart';
import 'package:kielegat_app/providers/maps_provider.dart';
import 'package:kielegat_app/providers/navigation_service.dart';
import 'package:kielegat_app/providers/news_provider.dart';
import 'package:kielegat_app/providers/sponsor_provider.dart';

GetIt locator = GetIt.I;

void setupLocator() {
  locator.registerLazySingleton(() => NewsProvider());
  locator.registerLazySingleton(() => NavigationService());
  locator.registerLazySingleton(() => GeneralInfoProvider());
  locator.registerLazySingleton(() => EventsProvider());
  locator.registerLazySingleton(() => MapsProvider());
  locator.registerLazySingleton(() => SponsorProvider());
  locator.registerLazySingleton(() => JukeboxProvider());
}
