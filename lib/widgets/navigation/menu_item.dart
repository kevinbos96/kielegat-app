import 'package:flutter/material.dart';
import 'package:kielegat_app/providers/navigation_service.dart';
import 'package:provider/provider.dart';

class MenuItem extends StatelessWidget {
  ValueChanged<bool> notifyParent;
  final Widget image;
  final Function func;
  final String route;

  MenuItem(this.image, {this.func, this.route});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: GestureDetector(
        onTap: route != null
            ? () {
                notifyParent(true);
                Provider.of<NavigationService>(context, listen : false).navigateTo(route);
              }
            : func,
        child: image,
      ),
    );
  }
}
