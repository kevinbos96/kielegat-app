import 'package:flutter/material.dart';
import 'package:kielegat_app/providers/navigation_service.dart';
import 'package:provider/provider.dart';

class AppLeadingWidget extends StatelessWidget {
  const AppLeadingWidget({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    NavigationService service = Provider.of<NavigationService>(context, listen : true);

    if (service.showLeading) {
      return IconButton(
        onPressed: () => service.goBack(),
        icon: Icon(Icons.arrow_back, color: Colors.white),
      );
    } else {
      return SizedBox.shrink();
    }
  }
}
