import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kielegat_app/common/kielegat_images.dart';
import 'package:kielegat_app/common/kielegat_style.dart';
import 'package:kielegat_app/widgets/navigation/menu_item.dart';

class OverlayMenu {
  final ValueChanged<bool> notifyParent;
  OverlayMenu(this.notifyParent);

  void _handleTap() {
    notifyParent(true);
  }

  OverlayEntry createOverlay(BuildContext context) {
    RenderBox renderBox = context.findRenderObject();
    var size = renderBox.size;
    var fullScreen = ScreenUtil().screenHeight;

    return OverlayEntry(
        builder: (context) => Positioned(
              left: 0,
              top: 0,
              width: size.width,
              height: fullScreen,
              child: Container(
                color: kielegat_blue.withOpacity(0.8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    body(context, fullScreen),
                  ],
                ),
              ),
            ));
  }

  Widget body(BuildContext context, double fullScreen) {
    double halfScreen = fullScreen * 0.5;
    double navigationScreen = halfScreen;
    double closeScreen = ScreenUtil().setHeight(40);

    return Material(
        color: kielegat_blue,
        child: Container(
          width: ScreenUtil().screenWidth,
          height: navigationScreen,
          padding: EdgeInsets.all(10),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Container(
                  height: ScreenUtil().setHeight(300),
                  alignment: Alignment(0.0, 0.0),
                  child: GridView.count(
                    physics: const NeverScrollableScrollPhysics(),
                    primary: true,
                    crossAxisCount: 4,
                    childAspectRatio: 5 / 4,
                    children: menu_overlay_items.map((MenuItem image) {
                      image.notifyParent = notifyParent;
                      return GridTile(
                        child: image,
                      );
                    }).toList(),
                  ),
                ),
                MaterialButton(
                  shape: CircleBorder(),
                  child: Icon(Icons.close),
                  height: closeScreen,
                  color: kielegat_black,
                  textColor: Colors.white,
                  onPressed: _handleTap,
                  padding: const EdgeInsets.all(15),
                ),
              ]),
        ));
  }
}
