import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kielegat_app/common/kielegat_style.dart';
import 'package:kielegat_app/routing/route_names.dart';

import 'bottom_nav_item.dart';
import 'overlay_menu.dart';

class BottomNav extends StatefulWidget {
  BottomNav({Key key}) : super(key: key);

  _BottomNavState createState() => _BottomNavState();
}

class _BottomNavState extends State<BottomNav> {
  bool hasFocus = false;

  OverlayEntry _overlayEntry;

  updateState(bool tst) {
    setState(() {
      if (!hasFocus) {
        hasFocus = true;
        Overlay.of(context).insert(this._overlayEntry);
      } else {
        hasFocus = false;
        this._overlayEntry.remove();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ScreenUtil().setHeight(60),
      color: kielegat_orange,
      child: Row(
        children: <Widget>[
          Expanded(
            flex: 3,
            child: NavBarItem(
              "HOME",
              'images/svg/home_wit.svg',
              HomeRoute,
            ),
          ),
          Expanded(
            flex: 3,
            child: NavBarItem(
              "NIEUWS",
              'images/svg/nieuws_wit.svg',
              NewsRoute,
            ),
          ),
          Expanded(
            flex: 2,
            child: GestureDetector(
              onTap: () {
                _overlayEntry = OverlayMenu(updateState).createOverlay(context);
                updateState(true);
              },
              child: Container(
                color: kielegat_blue,
                height: ScreenUtil().setHeight(70),
                child: Icon(
                  Icons.menu,
                  color: Colors.white,
                  size: 35,
                ),
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: NavBarItem(
              "EVENTS",
              'images/svg/evenementen_wit.svg',
              EventsRoute,
            ),
          ),
          Expanded(
              flex: 3,
              child: NavBarItem("WANDELING",
                  'images/svg/kielegatse-kilometer wit.svg', Wandeling)),
        ],
      ),
    );
  }
}
