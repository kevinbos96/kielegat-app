import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';


class AppHeader extends StatelessWidget implements PreferredSizeWidget {
  @override
  Size get preferredSize => Size.fromHeight(50.0);

  final Widget svg = SvgPicture.asset(
    'images/svg/app-logo.svg',
    height: ScreenUtil().setHeight(40),
  );
  final Widget svgPolonaise1 = SvgPicture.asset(
    'images/svg/polonaise01.svg',
    height: ScreenUtil().setHeight(35),
  );
  final Widget svgPolonaise2 = SvgPicture.asset(
    'images/svg/polonaise02.svg',
    height: ScreenUtil().setHeight(35),
  );

  @override
  Widget build(BuildContext context) {
    return titleWidget();
  }

  Widget titleWidget() {
    return Container(
      height: 50,
      child: Row( 
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[svgPolonaise1, Padding(
          padding: const EdgeInsets.symmetric(horizontal: 7), 
          child: svg,
        ), svgPolonaise2],
      ),
    )
    ;
  }
}
