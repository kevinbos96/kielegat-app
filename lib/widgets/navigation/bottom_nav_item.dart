import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kielegat_app/common/kielegat_style.dart';
import 'package:kielegat_app/providers/navigation_service.dart';
import 'package:provider/provider.dart';

class NavBarItem extends StatelessWidget {
  final String title;
  final String navigationPath;
  final String iconData;
  final Function func;
  const NavBarItem(this.title, this.iconData, this.navigationPath, {this.func});

  @override
  Widget build(BuildContext context) {
    NavigationService service =
        Provider.of<NavigationService>(context, listen: true);
    print(navigationPath + " " + service.isSelected(navigationPath).toString());

    return GestureDetector(
        onTap: () {
          if (func != null) {
            func();
          } else {
            service.navigateTo(navigationPath);
          }
        },
        child: Padding(
          padding: const EdgeInsets.only(bottom: 3),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              SvgPicture.asset(
                iconData,
                color: !service.isSelected(navigationPath)
                    ? Colors.white
                    : kielegat_blue,
                height: ScreenUtil().setHeight(35),
              ),
              Expanded(
                child: AutoSizeText(title,
                    textScaleFactor: 1,
                    style: TextStyle(
                        color: !service.isSelected(navigationPath)
                            ? Colors.white
                            : kielegat_blue,
                        fontSize: 12)),
              )
            ],
          ),
        ));
  }
}
