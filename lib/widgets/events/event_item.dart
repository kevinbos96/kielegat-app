import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:kielegat_app/common/kielegat_style.dart';
import 'package:kielegat_app/common/url_launcher.dart';
import 'package:kielegat_app/models/eventItem.dart';
import 'package:kielegat_app/providers/navigation_service.dart';
import 'package:kielegat_app/routing/route_names.dart';
import 'package:provider/provider.dart';

class EventItemWidget extends StatelessWidget {
  final EventItem eventItem;

  EventItemWidget({@required this.eventItem});
  final formatDate = DateFormat('EEEE d MMMM');
  final formatTime = DateFormat(' HH:mm');
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16.0),
      color: Colors.white,
      child: GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () {
          Provider.of<NavigationService>(context, listen: false)
              .navigateTo(EventDetailRoute, content: eventItem);
        },
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            buildTitle(),
            buildDate(),
            buildLocationAndTime(),
            buildDescription(),
            Align(
              alignment: Alignment.bottomLeft,
              child: buildPriceButton(),
            ),
          ],
        ),
      ),
    );
  }

  Widget buildPriceButton() {
    if (eventItem.ticketUrl != '') {
      return MaterialButton(
        color: kielegat_blue,
        textColor: Colors.white,
        padding: EdgeInsets.all(7.0),
        splashColor: Colors.redAccent,
        child: Text(eventItem.price),
        onPressed: () => UrlLauncher().launchURL(eventItem.ticketUrl),
      );
    }
    return MaterialButton(
        color: kielegat_red,
        textColor:  Colors.white,
        padding: EdgeInsets.all(7.0),
        splashColor: kielegat_red,
        child: Text(eventItem.price),
        onPressed: () => {},
      );
  }

  Flexible buildDescription() {
    return Expanded(
      flex: 1,
      child: AutoSizeText(
        eventItem.description,
        overflow: TextOverflow.ellipsis,
        style: kielegat_text_style,
        maxLines: 2,
      ),
    );
  }

  Widget buildTitle() {
    return Container(
      child: AutoSizeText.rich(
        TextSpan(text: eventItem.title, style: kielegat_head23),
        textAlign: TextAlign.left,
        style: kielegat_head20,
        minFontSize: 18,
        maxLines: 2,
      ),
    );
  }

  Widget buildDate() {
    return Container(
      child: AutoSizeText(
        formatDate.format(eventItem.eventDate),
        textAlign: TextAlign.left,
        style: kielegat_head15,
        maxLines: 1,
      ),
    );
  }

  Widget buildLocationAndTime() {
    var locationAndTime =
        eventItem.location + " " + formatTime.format(eventItem.eventDate);
    return Container(
      child: AutoSizeText(
        locationAndTime,
        textAlign: TextAlign.left,
        style: kielegat_head15,
        maxLines: 1,
      ),
    );
  }
}
