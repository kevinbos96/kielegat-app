import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:kielegat_app/common/kielegat_images.dart';
import 'package:kielegat_app/common/kielegat_style.dart';
import 'package:kielegat_app/models/eventItem.dart';
import 'package:kielegat_app/widgets/html/htmlView.dart';
import 'package:kielegat_app/widgets/maps/google_maps_events.dart';

class EventDetail extends StatefulWidget {
  final event;

  const EventDetail({Key key, this.event}) : super(key: key);

  @override
  _EventDetailState createState() => _EventDetailState();
}

class _EventDetailState extends State<EventDetail> {
  EventItem event;

  @override
  void initState() {
    event = widget.event;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return (event != null)
        ? buildBase(child: buildHead())
        : Container(
            color: Colors.white,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
  }

  Widget buildBase({Widget child}) {
    return Padding(
      padding: kielegat_base_padding,
      child: Container(
        color: kielegat_background_white,
        child: child,
      ),
    );
  }

  Widget buildHead({Widget child}) {
    final formatDate = DateFormat('EEEE d MMMM');
    final formatTime = DateFormat(' HH:mm');
    return SingleChildScrollView(
      child: Padding(
        padding: kielegat_base_padding,
        child: Column(
          children: <Widget>[
            buildImageWithDate(),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: Text(
                event.title,
                style: kielegat_head20,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 6),
              child: Container(
                child: Column(children: [
                  Row(children: [
                    AutoSizeText(
                      formatDate.format(event.eventDate),
                      textAlign: TextAlign.left,
                      style: kielegat_head15,
                    ),
                  ]),
                  Row(
                    children: <Widget>[
                      AutoSizeText(
                        event.location,
                        textAlign: TextAlign.left,
                        style: kielegat_head15,
                        maxLines: 1,
                      ),
                      AutoSizeText(
                        formatTime.format(event.eventDate),
                        textAlign: TextAlign.left,
                        style: kielegat_head15,
                      ),
                    ],
                  )
                ]),
              ),
            ),
            buildContent(),
            Padding(
              padding: const EdgeInsets.only(top: 8.0),
              child: buildMaps(),
            )
          ],
        ),
      ),
    );
  }

  Widget buildImageWithDate() {
    // print("url: " + event.imageUrl);
    return Stack(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(top: 0), //top: 24
          child: Container(
            // height: double.,
            width: double.maxFinite,
            child: event.imageUrl != ""
                ? CachedNetworkImage(
                    imageUrl: event.imageUrl,
                    placeholder: (context, url) => CircularProgressIndicator(),
                    errorWidget: (context, url, error) => placeHolder,
                  )
                : Container(),
          ),
        ),
      ],
    );
  }

  Widget buildContent() {
    //todo HTML!
    return HtmlView(content: event.content);

    // return Html(
    //   data: event.content,
    //   defaultTextStyle: kielegat_text_style,
    //   useRichText: true,
    //   onLinkTap: (url) => UrlLauncher().launchURL(url),
    //   renderNewlines: true,
    //   onImageTap: (source) => PhotoView(
    //     imageProvider: NetworkImage(source),
    //   ),
    // );
  }

  Widget buildMaps() {
    Marker marker = Marker(
        markerId: MarkerId("Marker"),
        position:
            LatLng(event.eventLocation.latitude, event.eventLocation.longitude),
        infoWindow: InfoWindow(title: event.location));

    if (event.eventLocation.latitude == 1 &&
        event.eventLocation.longitude == 1) {
      return Container();
    } else {
      return Container(height: 300, child: MapsEventWidget(marker));
    }
  }
}
