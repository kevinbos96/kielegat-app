import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:kielegat_app/common/url_launcher.dart';

class HtmlView extends StatelessWidget {
  const HtmlView({Key key, @required this.content}) : super(key: key);

  final String content;

  @override
  Widget build(BuildContext context) {
    return Html(
      data: content,
      onLinkTap: (url) {
        UrlLauncher().launchURL(url);
      },
    );
  }
}
