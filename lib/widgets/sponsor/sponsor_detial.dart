import 'package:flutter/material.dart';
import 'package:kielegat_app/common/kielegat_style.dart';
import 'package:kielegat_app/common/url_launcher.dart';
import 'package:kielegat_app/models/sponsor.dart';
import 'package:kielegat_app/models/sponsor_group.dart';

class SponsorDetial extends StatelessWidget {
  final SponsorGroup sponsorGroup;

  const SponsorDetial(this.sponsorGroup);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Container(
        color: kielegat_background_white,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: buildRows(),
          ),
        ),
      ),
    );
  }

  List<Widget> buildRows() {
    List<Widget> rows = List<Widget>();
    rows.add(Padding(
      padding: const EdgeInsets.only(top: 0.0, bottom: 8.0),
      child: Text(
        sponsorGroup.name,
        style: kielegat_head30,
        textAlign: TextAlign.left,
      ),
    ));

    for (Sponsor sponsor in sponsorGroup.sponsorList) {
      rows.add(GestureDetector(
        onTap: (){
          UrlLauncher().launchURL(sponsor.website);
          // print(sponsor.name);
        },
          child: Text(
        sponsor.name,
        style: kielegat_text_20_style,
        textAlign: TextAlign.left,
      )));
    }

    return rows;
  }
}
