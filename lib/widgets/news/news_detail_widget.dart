import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:kielegat_app/common/date_format.dart';
import 'package:kielegat_app/common/kielegat_images.dart';
import 'package:kielegat_app/common/kielegat_style.dart';
import 'package:kielegat_app/common/url_launcher.dart';
import 'package:kielegat_app/models/article.dart';
import 'package:kielegat_app/widgets/html/htmlView.dart';
import 'package:photo_view/photo_view.dart';

import 'news_date_widget.dart';

class NewsDetail extends StatefulWidget {
  final article;

  const NewsDetail({Key key, this.article}) : super(key: key);

  @override
  _NewsDetailState createState() => _NewsDetailState();
}

class _NewsDetailState extends State<NewsDetail> {
  Article article;
  PhotoView photoView;

  @override
  void initState() {
    article = widget.article;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return (article != null)
        ? buildBase(child: buildHead())
        : Container(
            color: Colors.white,
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
  }

  Widget buildBase({Widget child}) {
    return Padding(
      padding: kielegat_base_padding,
      child: Container(
        color: kielegat_background_white,
        child: child,
      ),
    );
  }

  Widget buildHead({Widget child}) {
    return SingleChildScrollView(
      child: Stack(children: [
        Padding(
          padding: kielegat_base_padding,
          child: Column(
            children: <Widget>[
              buildImageWithDate(),
              Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: AutoSizeText(
                    article.title,
                    maxLines: 2,
                    style: kielegat_head20,
                  )),
              Padding(
                padding: const EdgeInsets.only(top: 8.0),
                child: Container(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      "Gepubliceerd op: " + dateToString(article.datePublished),
                      style: kielegat_small12,
                      textAlign: TextAlign.left,
                    )),
              ),
              buildContent()
            ],
          ),
        ),
        NewsDate(
          article: article,
        ),
      ]),
    );
  }

  Widget buildImageWithDate() {
    return Padding(
      padding: const EdgeInsets.only(top: 0), //top: 24
      child: Container(
        // height: double.,
        width: double.maxFinite,
        child: CachedNetworkImage(
          imageUrl: article.imageUrl,
          placeholder: (context, url) => CircularProgressIndicator(),
          errorWidget: (context, url, error) => placeHolder,
        ),
      ),
    );
  }

  Widget buildContent() {
    //todo HTML!

    return HtmlView(content: article.content);

    // return Html(
    //     data: article.content,
    //     defaultTextStyle: kielegat_text_style,
    //     useRichText: true,
    //     onLinkTap: (url) => UrlLauncher().launchURL(url),
    //     renderNewlines: true,
    //     onImageTap: (source) => {
    //           photoView = PhotoView(
    //             imageProvider: NetworkImage(source),
    //           )
    //         });
  }
}
