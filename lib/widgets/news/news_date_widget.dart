import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:kielegat_app/common/kielegat_style.dart';
import 'package:kielegat_app/common/news_style.dart';
import 'package:kielegat_app/models/article.dart';

class NewsDate extends StatelessWidget {
  final Article article;

  NewsDate({@required this.article});

  @override
  Widget build(BuildContext context) {
    return Positioned(
        right: 36.0,
        top: 8.0,
        child: Container(
            width: 50.0,
            height: 60.0,
            decoration: BoxDecoration(color: kielegat_red),
            child: Column(
              children: <Widget>[
                buildAutoSize(article.datePublished.day.toString()),
                buildAutoSize(article.datePublished.month.toString()),
                buildAutoSize(article.datePublished.year.toString())
              ],
            )));
  }

  Widget buildAutoSize(String text) {
    return Expanded(
      child: AutoSizeText(
        text,
        maxLines: 1,
        stepGranularity: 1,
        style: newsDateYear,
        textScaleFactor: 1,
      ),
    );
  }
}
