import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:kielegat_app/common/kielegat_images.dart';
import 'package:kielegat_app/common/kielegat_style.dart';
import 'package:kielegat_app/models/article.dart';
import 'package:kielegat_app/providers/navigation_service.dart';
import 'package:kielegat_app/routing/route_names.dart';
import 'package:provider/provider.dart';

import 'news_date_widget.dart';

class NewsItem extends StatelessWidget {
  final Article article;

  NewsItem({@required this.article});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Stack(children: [
        GestureDetector(
          behavior: HitTestBehavior.translucent,
          onTap: () {
            Provider.of<NavigationService>(context, listen : false)
                .navigateTo(NewsDetailRoute, content: article);
          },
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(
              children: <Widget>[
                buildImage(),
                buildTitle(),
                buildDescription()
              ],
            ),
          ),
        ),
        NewsDate(
          article: article,
        )
      ]),
    );
  }

  Widget buildImage() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8),
      child: ConstrainedBox(
        constraints: BoxConstraints(maxHeight: 150),
        child: CachedNetworkImage(
          imageUrl: article.imageUrl,
          placeholder: (context, url) => CircularProgressIndicator(),
          errorWidget: (context, url, error) => placeHolder,
        ),
      ),
    );
  }

  Widget buildTitle() {
    return Container(
      child: AutoSizeText(
        article.title,
        maxLines: 2,
        style: kielegat_head20,
      ),
    );
  }

  Widget buildDescription() {
    return Expanded(
      child: AutoSizeText(
        article.description,
        maxLines: 4,
        style: kielegat_text_style,
        overflow: TextOverflow.ellipsis,
      ),
    );
  }

  Widget buildOldCard(BuildContext context) {
    return Container(
      color: Colors.white,
      child: GestureDetector(
        onTap: () {
          Provider.of<NavigationService>(context, listen : false)
              .navigateTo(NewsDetailRoute, content: article);
        },
        child: Stack(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                children: <Widget>[
                  ConstrainedBox(
                    constraints: BoxConstraints(maxHeight: 155),
                    child: CachedNetworkImage(
                      imageUrl: article.imageUrl,
                      placeholder: (context, url) =>
                          CircularProgressIndicator(),
                      errorWidget: (context, url, error) => placeHolder,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8),
                    child: Container(
                        alignment: Alignment(-1.0, 0),
                        child: AutoSizeText(
                          article.title,
                          maxLines: 2,
                          style: kielegat_head16,
                        )),
                  ),
                  Expanded(
                    child: AutoSizeText(
                      article.description,
                      maxLines: 2,
                      style: kielegat_text_style,
                    ),
                  )
                ],
              ),
            ),
            NewsDate(
              article: article,
            ),
          ],
        ),
      ),
    );
  }
}
