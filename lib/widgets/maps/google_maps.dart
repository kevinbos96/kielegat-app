import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:kielegat_app/locator.dart';
import 'package:kielegat_app/models/map_item.dart';
import 'package:kielegat_app/providers/maps_provider.dart';
import 'package:location/location.dart';

class MapsWidget extends StatefulWidget {
  final String pageId;

  const MapsWidget({Key key, @required this.pageId}) : super(key: key);

  @override
  _MapsWidgetState createState() => _MapsWidgetState();
}

class _MapsWidgetState extends State<MapsWidget> {
  final Completer<GoogleMapController> _mapController = Completer();

  /// Set of displayed markers and cluster markers on the map
  final Set<Marker> _markers = Set();

  final Set<Polyline> _polylines = Set();
  MapItem _mapItem;
  List<LatLng> polylineCoordinates = [];

  /// Current map zoom. Initial zoom will be 15, street level
  double _currentZoom = 15;
  GeoPoint initPosition = GeoPoint(51.5865421, 4.77459959999999);
  bool finished = false;

  @override
  void initState() {
    getLocationPermission();
    // locator<MapsProvider>()
    //     .fetchMarkers(widget.pageId)
    //     .then((locations) => {_initMarkers(locations)});

    locator<MapsProvider>().fetchMapItem(widget.pageId).then((locations) => {
          _mapItem = locations,
          setPolylines(),
          _currentZoom = locations.initialZoom.toDouble(),
          initPosition = locations.centerPoint,
          finished = true
        }); //_initPolyline(locations)
    super.initState();
  }

  /// Called when the Google Map widget is created. Updates the map loading state
  /// and inits the markers.
  void _onMapCreated(GoogleMapController controller) {
    _mapController.complete(controller);

    setState(() {});
  }

  void setPolylines() async {
    Set<Marker> mark = await _mapItem.toMarkers();
    setState(() {
      // create a Polyline instance
      // with an id, an RGB color and the list of LatLng pairs
      Polyline polyline = Polyline(
          polylineId: PolylineId("poly"),
          color: Color.fromARGB(255, 40, 122, 198),
          points: _mapItem.toPoints());

      // add the constructed polyline as a set of points
      // to the polyline set, which will eventually
      // end up showing up on the map
      _polylines.add(polyline);
      _markers.addAll(mark);
    });
  }

  @override
  Widget build(BuildContext context) {
    return finished == false
        ? Dialog(
            child: Center(child: CircularProgressIndicator()),
          )
        : Container(
            // Google Map widget
            child: GoogleMap(
              myLocationButtonEnabled: true,
              myLocationEnabled: true,
              mapToolbarEnabled: true,
              initialCameraPosition: CameraPosition(
                target: LatLng(initPosition.latitude, initPosition.longitude),
                zoom: _currentZoom,
              ),
              markers: _markers,
              polylines: _polylines,
              onMapCreated: (controller) => _onMapCreated(controller),
              gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>[
                Factory<OneSequenceGestureRecognizer>(
                  () => EagerGestureRecognizer(),
                ),
              ].toSet(),
            ),
          );
  }

  getLocationPermission() async {
    var location = Location();
    try {
      await location.requestPermission(); //to lunch location permission popup
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        print('Permission denied');
      }
    }
  }
}
