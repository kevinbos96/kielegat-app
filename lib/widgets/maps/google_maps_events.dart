import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class MapsEventWidget extends StatefulWidget {
  final Marker marker;
  MapsEventWidget(this.marker);

  @override
  _MapsEventWidget createState() => _MapsEventWidget();
}

class _MapsEventWidget extends State<MapsEventWidget> {
  final Completer<GoogleMapController> _mapController = Completer();

  double _currentZoom = 15;

  @override
  void initState() {
    super.initState();
    getLocationPermission();
  }

  void _onMapCreated(GoogleMapController controller) {
    _mapController.complete(controller);

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      // Google Map widget
      child: GoogleMap(
        myLocationButtonEnabled: true,
        myLocationEnabled: true,
        mapToolbarEnabled: true,
        initialCameraPosition: CameraPosition(
          target: widget.marker.position,
          zoom: _currentZoom,
        ),
        markers: [widget.marker].toSet(),
        onMapCreated: (controller) => _onMapCreated(controller),
        gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>[
          Factory<OneSequenceGestureRecognizer>(
            () => EagerGestureRecognizer(),
          ),
        ].toSet(),
      ),
    );
  }

  getLocationPermission() async {
    var location = Location();
    try {
      await location.requestPermission(); //to lunch location permission popup
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        print('Permission denied');
      }
    }
  }
}
