import 'dart:async';
import 'dart:io';

import 'package:audioplayers/audioplayers.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:kielegat_app/common/kielegat_style.dart';
import 'package:kielegat_app/models/jukebox.dart';
import 'package:kielegat_app/providers/jukebox_state_provider.dart';
import 'package:provider/provider.dart';

class JukeboxItemWidget extends StatefulWidget {
  final Jukebox jukebox;

  JukeboxItemWidget(this.jukebox);
  @override
  _JukeboxItemWidgetState createState() => _JukeboxItemWidgetState();
}

enum Buffer { none, running, error }

class _JukeboxItemWidgetState extends State<JukeboxItemWidget> {
  JukeboxStateProvider jukeboxState;

  AudioPlayer audioPlayer;
  AudioPlayerState playerState = AudioPlayerState.STOPPED;

  Duration duration = Duration(seconds: 0);
  Duration position = Duration(seconds: 0);
  Buffer buffer = Buffer.none;

  StreamSubscription _positionSubscription;
  StreamSubscription _audioPlayerStateSubscription;

  get isPlaying => playerState == AudioPlayerState.PLAYING;

  @override
  void initState() {
    super.initState();
  }

  void initAudioPlayer() {
    jukeboxState.SetState(widget.jukebox.documentId);
    audioPlayer = AudioPlayer();
    audioPlayer.onDurationChanged.listen((Duration d) {
      setState(() => duration = d);
    });
    _positionSubscription = audioPlayer.onAudioPositionChanged
        .listen((p) => setState(() => {position = p, buffer = Buffer.none}));
    _audioPlayerStateSubscription =
        audioPlayer.onPlayerStateChanged.listen((s) {
      if (s == AudioPlayerState.PLAYING) {
      } else if (s == AudioPlayerState.STOPPED) {
        onComplete();
        setState(() {
          position = duration;
        });
      }
    }, onError: (msg) {
      setState(() {
        playerState = AudioPlayerState.STOPPED;
        duration = Duration(seconds: 0);
        position = Duration(seconds: 0);
        buffer = Buffer.error;
      });
    });
  }

  @override
  void dispose() {
    _unsubescribeListeners();
    if (audioPlayer != null) {
      audioPlayer.stop();
      audioPlayer.release();
    }
    super.dispose();
  }

  @override
  void deactivate() {
     _unsubescribeListeners();
    if (audioPlayer != null) {
      audioPlayer.stop();
    }
    super.deactivate();
  }

  

  void onComplete() {
    setState(() => playerState = AudioPlayerState.STOPPED);
  }

  Future<void> _play() async {
    if (jukeboxState.id != widget.jukebox.documentId) {
      initAudioPlayer();
      await stop();
    }

    setState(() => {buffer = Buffer.running});

    try {
      File file = await DefaultCacheManager().getSingleFile(widget.jukebox.url);
      await audioPlayer.play(file.path);
      setState(
          () => {playerState = AudioPlayerState.PLAYING, buffer = Buffer.none});
    } catch (e) {
      setState(() => {buffer = Buffer.error});
    }
  }

  Future<void> pause() async {
    await audioPlayer.pause();
    setState(() => playerState = AudioPlayerState.PAUSED);
  }

  Future<void> stop() async {
    await audioPlayer.stop();
    setState(() {
      playerState = AudioPlayerState.STOPPED;
      position = Duration();
    });
  }

  _seekToSecond(int second) async {
    if (audioPlayer != null) {
      await audioPlayer.seek(Duration(seconds: second));
    }
  }

  _buttonPressed() {
    if (playerState == AudioPlayerState.STOPPED ||
        playerState == AudioPlayerState.PAUSED) {
      _play();
    } else if (playerState == AudioPlayerState.PLAYING) {
      pause();
    }
  }

  _unsubescribeListeners() async {
    if (_positionSubscription != null) {
      await _positionSubscription.cancel();
      await _audioPlayerStateSubscription.cancel();
    }
  }

  @override
  Widget build(BuildContext context) {
    jukeboxState = Provider.of<JukeboxStateProvider>(context, listen: true);
    if (jukeboxState.id != widget.jukebox.documentId) {
      stop();
      _unsubescribeListeners();
      if (playerState == AudioPlayerState.PLAYING) {
        playerState = AudioPlayerState.PAUSED;
      }
    }

    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Container(
        color: kielegat_background_white,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              AutoSizeText(
                widget.jukebox.title + " - " + widget.jukebox.artiest,
                style: kielegat_head20,
                maxLines: 2,
              ),
              buildPlayer()
            ],
          ),
        ),
      ),
    );
  }

  Widget buildPlayer() {
    return Container(
      // height: 75,
      child: Row(children: [
        GestureDetector(
            onTap: () {
              _buttonPressed();
            },
            child: buildState()),
        Expanded(
          child: Slider(
              value: position.inSeconds.toDouble(),
              min: 0,
              max: duration.inSeconds.toDouble(),
              onChanged: (value) => {
                    setState(() {
                      // pos = value;
                      _seekToSecond(value.toInt());
                    })
                  }),
        ),
      ]),
    );
  }

  Widget buildState() {
    //als state == playing && buffer is running
    //return cicrularProgress
    //als buffer == error
    //return error icon
    //alse state == playing
    //rertun play pause
    //else
    //return play

    if (buffer == Buffer.running) {
      return CircularProgressIndicator();
    } else if (buffer == Buffer.error) {
      return Icon(Icons.error, size: 30);
    } else {
      return Icon(
        playerState == AudioPlayerState.PLAYING
            ? Icons.pause
            : Icons.play_arrow,
        size: 30,
      );
    }
  }
}
