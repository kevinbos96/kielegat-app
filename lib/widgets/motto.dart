import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:kielegat_app/common/kielegat_images.dart';
import 'package:kielegat_app/providers/general_info_provider.dart';

import '../locator.dart';

class MottoLogoWidget extends StatefulWidget {
  const MottoLogoWidget({Key key}) : super(key: key);

  @override
  _MottoLogoWidgetState createState() => _MottoLogoWidgetState();
}

class _MottoLogoWidgetState extends State<MottoLogoWidget>
    with SingleTickerProviderStateMixin {
  AnimationController _mottoController;
  String mottoUrl;
  bool rotating = false;

  @override
  void initState() {
    super.initState();
    _mottoController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 100),
    );
    _mottoController.repeat();

    locator<GeneralInfoProvider>().getPageById('Motto').then((page) => {
          setState(
              () => {mottoUrl = page.url, rotating = page.rotating, _roating()})
        });
  }

  _roating() {
    if (!rotating) {
      _mottoController.stop();
      _mottoController.reset();
    }
  }

  @override
  Widget build(BuildContext context) {
    double speed = 50;
    int direction = 1;

    return GestureDetector(
      onDoubleTap: () => direction *= -1,
      child: Container(
          padding: EdgeInsets.only(top: ScreenUtil().setWidth(40)),
          child: AnimatedBuilder(
            animation: _mottoController,
            child: Container(
                height: ScreenUtil().setHeight(200),
                width: ScreenUtil().setHeight(200),
                child: mottoUrl != null
                    ? CachedNetworkImage(
                        imageUrl: mottoUrl,
                        placeholder: (context, url) =>
                            CircularProgressIndicator(),
                        errorWidget: (context, url, error) => placeHolder,
                      )
                    : placeHolder),
            builder: (BuildContext context, Widget _widget) {
              return Transform.rotate(
                angle: _mottoController.value * speed * direction,
                child: _widget,
              );
            },
          )),
    );
  }

  @override
  void dispose() {
    _mottoController.dispose();
    super.dispose();
  }
}
