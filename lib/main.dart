import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';
import 'package:kielegat_app/pages/base_page.dart';
import 'package:kielegat_app/providers/navigation_service.dart';
import 'package:provider/provider.dart';

import 'locator.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  setupLocator();
  await initializeDateFormatting();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    Intl.defaultLocale = 'nl';
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<NavigationService>.value(
          value: NavigationService(),
        ),
      ],
      child: MaterialApp(
        title: 'Kielegat',
        theme: ThemeData(primarySwatch: Colors.orange, fontFamily: 'Lato'),
        home: BasePage(),
      ),
    );
  }
}
