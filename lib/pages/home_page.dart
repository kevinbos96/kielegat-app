import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kielegat_app/common/kielegat_images.dart';
import 'package:kielegat_app/common/url_launcher.dart';
import 'package:kielegat_app/locator.dart';
import 'package:kielegat_app/providers/general_info_provider.dart';
import 'package:kielegat_app/providers/navigation_service.dart';
import 'package:kielegat_app/routing/route_names.dart';
import 'package:kielegat_app/widgets/motto.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  bool _isLoading = false;
  String kieleshop = "";
  String sanseveria = "";
  String carnavalsMenu = "";
  String jukebox = "";

  @override
  void initState() {
    locator<GeneralInfoProvider>().initialize();
    locator<GeneralInfoProvider>().getPageById('kieleshop').then((page) => {
          setState(() => {kieleshop = page.url})
        });
    locator<GeneralInfoProvider>().getPageById('sanseveria').then((page) => {
          setState(() => {sanseveria = page.url})
        });
    locator<GeneralInfoProvider>().getPageById('CarnavalsMenu').then((page) => {
          setState(() => {carnavalsMenu = page.url})
        });
    locator<GeneralInfoProvider>().getPageById('jukebox').then((page) => {
          setState(() => {jukebox = page.url})
        });

    // kieleshop = tester.getKieleshop();

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return !_isLoading
        ? SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(bottom: ScreenUtil().setWidth(40)),
                  child: MottoLogoWidget(),
                ),
                buildGrid([
                  buildNavigationButton(NewsRoute, 0),
                  buildNavigationButton(EventsRoute, 1),
                  buildUrlButton(kieleshop, 3)
                ]),
                buildGrid([
                  buildNavigationButton(SponsorRoute, 4),
                  buildUrlButton(jukebox, 13),
                  buildNavigationButton(LeutpenningenRoute, 6)
                ]),
                buildGrid([
                  buildUrlButton(sanseveria, 15),
                  buildUrlButton(carnavalsMenu, 16),
                  buildNavigationButton(Wandeling, 17),
                ]),
                Container(
                  height: 16,
                )
              ],
            ),
          )
        : Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
  }

  Widget buildGrid(List<Widget> children) {
    return Container(
        alignment: Alignment(0.0, 0.0),
        padding: EdgeInsets.only(top: ScreenUtil().setWidth(8)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: children,
        ));
  }

  Widget buildButton(int index, Function onTab) {
    return GestureDetector(
      onTap: onTab,
      child: Container(padding: EdgeInsets.all(6), child: menu_items[index]),
    );
  }

  Widget buildNavigationButton(String location, int index) {
    return buildButton(
        index,
        () => Provider.of<NavigationService>(context, listen: false)
            .navigateTo(location));
  }

  Widget buildUrlButton(String url, index) {
    return buildButton(index, () => UrlLauncher().launchURL(url));
  }
}
