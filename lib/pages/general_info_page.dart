import 'package:auto_size_text/auto_size_text.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:kielegat_app/common/kielegat_style.dart';
import 'package:kielegat_app/locator.dart';
import 'package:kielegat_app/models/customPage.dart';
import 'package:kielegat_app/providers/general_info_provider.dart';
import 'package:kielegat_app/providers/navigation_service.dart';
import 'package:kielegat_app/routing/route_names.dart';
import 'package:kielegat_app/widgets/html/htmlView.dart';
import 'package:kielegat_app/widgets/maps/google_maps.dart';
import 'package:provider/provider.dart';

class GeneralInfoPage extends StatefulWidget {
  final String pageId;
  final bool maps;
  final bool disablePadding;

  const GeneralInfoPage(
      {Key key, this.pageId, this.maps = false, this.disablePadding = false})
      : super(key: key);

  @override
  _GeneralInfoPageState createState() => _GeneralInfoPageState();
}

class _GeneralInfoPageState extends State<GeneralInfoPage> {
  Future<CustomPage> page;
  @override
  void initState() {
    setLoading(true);
    page = locator<GeneralInfoProvider>().getPageById(widget.pageId);
    super.initState();
  }

  void setLoading(bool state) {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<CustomPage>(
        future: page,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            if (snapshot.data.content != "") {
              return buildWithContent(snapshot.data);
            } else {
              return MapsWidget(pageId: widget.pageId);
            }
          } else {
            return Text('Leutige items ophalen');
          }
        });
  }

  Widget buildWithContent(CustomPage page) {
    EdgeInsets base = kielegat_base_padding;
    if (widget.disablePadding) {
      base = EdgeInsets.all(0);
    }

    return SingleChildScrollView(
      child: Padding(
        padding: base,
        child: Container(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: <Widget>[
                buildImage(page),
                AutoSizeText(
                  page.title,
                  style: kielegat_head20,
                  maxLines: 2,
                ),
                buildContent(page),
                buildSmallMap(page)
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildContent(CustomPage page) {
    return HtmlView(content: page.content);

    // return Html(
    //   data: page.content,
    //   style: kielegat_text_style,
    //   onLinkTap: (url) => UrlLauncher().launchURL(url),
    // );
  }

  Widget buildMapImage(CustomPage page) {
    if (page.hasLocations) {
      if (page.mapImageUrl != "") {
        return Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: buildStaticMapImage(page));
      } else {
        return buildButtonMap(page);
      }
    } else {
      return Container();
    }
  }

  Widget buildImage(CustomPage page) {
    return page.imageUrl != ""
        ? CachedNetworkImage(
            imageUrl: page.imageUrl,
            placeholder: (context, url) => CircularProgressIndicator(),
            errorWidget: (context, url, error) => Container(),
          )
        : Container();
  }

  Widget buildStaticMapImage(CustomPage page) {
    return GestureDetector(
      onTap: () {
        Provider.of<NavigationService>(context, listen: false)
            .navigateTo(GoogleMapsRoute, content: widget.pageId);
      },
      child: CachedNetworkImage(
        imageUrl: page.mapImageUrl,
        placeholder: (context, url) => CircularProgressIndicator(),
        errorWidget: (context, url, error) => buildButtonMap(page),
      ),
    );
  }

  Widget buildButtonMap(CustomPage page) {
    return MaterialButton(
      color: kielegat_blue,
      textColor: Colors.white,
      padding: EdgeInsets.all(7.0),
      splashColor: Colors.redAccent,
      child: Text("Bekijk op de kaart"),
      onPressed: () {
        Provider.of<NavigationService>(context, listen: false)
            .navigateTo(GoogleMapsRoute, content: widget.pageId);
      },
    );
  }

  Widget buildSmallMap(CustomPage page) {
    return page.hasLocations
        ? Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Container(
              height: 300,
              child: MapsWidget(pageId: widget.pageId),
            ),
          )
        : Container();
  }
}
