import 'package:flutter/material.dart';
import 'package:kielegat_app/widgets/maps/google_maps.dart';

class MapsPage extends StatelessWidget {
  final String pageId;

  const MapsPage({Key key, this.pageId}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return Container(
      child: MapsWidget(pageId: pageId),
    );
  }
}