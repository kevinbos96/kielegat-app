import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:kielegat_app/common/kielegat_style.dart';
import 'package:kielegat_app/locator.dart';
import 'package:kielegat_app/models/jukebox.dart';
import 'package:kielegat_app/pages/general_info_page.dart';
import 'package:kielegat_app/providers/jukebox_provider.dart';
import 'package:kielegat_app/providers/jukebox_state_provider.dart';
import 'package:kielegat_app/routing/route_names.dart';
import 'package:kielegat_app/widgets/jukebox/jukebox_item.dart';
import 'package:provider/provider.dart';

class JukeBoxPage extends StatefulWidget {
  const JukeBoxPage({Key key}) : super(key: key);

  @override
  _JukeBoxPageState createState() => _JukeBoxPageState();
}

class _JukeBoxPageState extends State<JukeBoxPage> {
  final jukeboxProvider = locator<JukeboxProvider>();

  List<Jukebox> _jukeboxes;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: jukeboxProvider.fetchJukeBox(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasData) {
          _jukeboxes = snapshot.data.docs
              .map((doc) => Jukebox.fromMap(doc.data(), doc.id))
              .toList();

          return ChangeNotifierProvider<JukeboxStateProvider>(
            create: (context) => JukeboxStateProvider(),
            child: Padding(
              padding: kielegat_base_padding,
              child: ListView.builder(
                itemCount: _jukeboxes.length + 1,
                itemBuilder: (context, index) {
                  if (index == 0) {
                    return buildInfoWidget();
                  } else {
                    return JukeboxItemWidget(_jukeboxes[index - 1]);
                  }
                },
              ),
            ),
          );
        } else {
          return Text("Muzikale leut ophalen");
        }
      },
    );
  }

  Widget buildInfoWidget() {
    return GeneralInfoPage(
      pageId: JukeBoxRoute,
      disablePadding: true,
    );
  }
}
