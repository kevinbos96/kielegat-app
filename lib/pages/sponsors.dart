import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:kielegat_app/common/kielegat_style.dart';
import 'package:kielegat_app/models/sponsor_group.dart';
import 'package:kielegat_app/providers/sponsor_provider.dart';
import 'package:kielegat_app/widgets/sponsor/sponsor_detial.dart';

import '../locator.dart';

class SponsorPage extends StatefulWidget {
  SponsorPage({Key key}) : super(key: key);

  @override
  _SponsorPageState createState() => _SponsorPageState();
}

class _SponsorPageState extends State<SponsorPage> {
  List<SponsorGroup> _sponsorGroup;

  final sponsorProvider = locator<SponsorProvider>();

  @override
  Widget build(BuildContext context) {
    return Container(
      child: StreamBuilder(
        stream: sponsorProvider.fetchSponsorsAsStream(),
        builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasData) {
            _sponsorGroup = snapshot.data.documents
                .map((doc) => SponsorGroup.fromMap(doc.data(), doc.id))
                .toList();
            return Padding(
              padding: kielegat_base_padding,
              child: ListView.builder(
                itemCount: _sponsorGroup.length,
                itemBuilder: (context, index) =>
                    SponsorDetial(_sponsorGroup[index]),
              ),
            );
          } else {
            return Container();
          }
        },
      ),
    );
  }
}
