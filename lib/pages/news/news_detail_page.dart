import 'package:flutter/material.dart';
import 'package:kielegat_app/models/article.dart';
import 'package:kielegat_app/widgets/news/news_detail_widget.dart';

class NewsDetailPage extends StatelessWidget {
  final article;
  const NewsDetailPage(this.article, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return NewsDetail(article: article as Article);
  }
}
