import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:kielegat_app/common/kielegat_style.dart';
import 'package:kielegat_app/locator.dart';
import 'package:kielegat_app/models/article.dart';
import 'package:kielegat_app/providers/news_provider.dart';
import 'package:kielegat_app/widgets/news/news_item.dart';

class NewsOverview extends StatefulWidget {
  @override
  _NewsOverviewState createState() => _NewsOverviewState();
}

class _NewsOverviewState extends State<NewsOverview> {
  List<Article> articleList;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final articlesNew = locator<NewsProvider>();

    return Container(
      child: StreamBuilder(
          stream: articlesNew.fetchArticlesAsStream(),
          builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasData) {
              articleList = snapshot.data.docs
                  .map((doc) => Article.fromMap(doc.data(), doc.id))
                  .toList();
              articleList
                  .sort((a, b) => b.datePublished.compareTo(a.datePublished));

              return GridView.builder(
                padding: kielegat_base_padding,
                itemCount: articleList.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 1,
                    childAspectRatio: 5 / 4,
                    crossAxisSpacing: 10,
                    mainAxisSpacing: 10),
                itemBuilder: (buildContext, index) =>
                    NewsItem(article: articleList[index]),
              );
            } else {
              return Text('Leutig nieuws ophalen');
            }
          }),
    );
  }
}
