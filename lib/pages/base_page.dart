import 'package:auto_size_text/auto_size_text.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:giffy_dialog/giffy_dialog.dart';
import 'package:kielegat_app/common/custom_clipper.dart';
import 'package:kielegat_app/common/screen_util.dart';
import 'package:kielegat_app/providers/navigation_observer.dart';
import 'package:kielegat_app/providers/navigation_service.dart';
import 'package:kielegat_app/routing/route_names.dart';
import 'package:kielegat_app/routing/router.dart';
import 'package:kielegat_app/widgets/motto.dart';
import 'package:kielegat_app/widgets/navigation/app_header.dart';
import 'package:kielegat_app/widgets/navigation/app_leading.dart';
import 'package:kielegat_app/widgets/navigation/bottom_nav.dart';
import 'package:kielegat_app/common/kielegat_style.dart';
import 'package:kielegat_app/widgets/notification/notification_dialog.dart';
import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:provider/provider.dart';

class BasePage extends StatefulWidget {
  BasePage() : super();

  final String title = "Kielegat app";

  @override
  _BasePageState createState() => _BasePageState();
}

class _BasePageState extends State<BasePage> {
  int currentIndex = 0;
  bool ifPop;

  bool myInterceptor(bool stopDefaultButtonEvent, RouteInfo info) {
    if (stopDefaultButtonEvent) return false;
    pop();
    return true;
  }

  final FirebaseMessaging _fcm = FirebaseMessaging();

  @override
  void initState() {
    super.initState();
    ifPop = false;
    BackButtonInterceptor.add(myInterceptor, name: "test");

    Map<String, dynamic> fixMessageTitleAndBody(Map<String, dynamic> message) {
      if (!message.containsKey("notification")) {
        message["notification"] = {};
      }
      if (!message["notification"].containsKey("title") &&
          message["data"].containsKey("title")) {
        message["notification"]["title"] = message["data"]["title"];
      }
      if (!message["notification"].containsKey("body") &&
          message["data"].containsKey("body")) {
        message["notification"]["body"] = message["data"]["body"];
      }
      return message;
    }

    _fcm.requestNotificationPermissions(IosNotificationSettings());
    _fcm.getToken().then((String token) {
      print("Push Messaging token: $token");
      // Push messaging to this token later
    });
    _fcm.configure(
      onMessage: (Map<String, dynamic> message) async {
        await showDialog(
            context: context,
            builder: (_) => NotificationDialog(
                  imageWidget: MottoLogoWidget(),
                  title: Text(
                    message['notification']['title'],
                    style:
                        TextStyle(fontSize: 22.0, fontWeight: FontWeight.w600),
                  ),
                  description: AutoSizeText(message['notification']['body']),
                  entryAnimation: EntryAnimation.TOP_RIGHT,
                ));
      },
      onResume: (Map<String, dynamic> message) async {
        message = fixMessageTitleAndBody(message);
        await showDialog(
            context: context,
            builder: (_) => NotificationDialog(
                  imageWidget: MottoLogoWidget(),
                  title: Text(
                    message['notification']['title'],
                    style:
                        TextStyle(fontSize: 22.0, fontWeight: FontWeight.w600),
                  ),
                  description: AutoSizeText(message['notification']['body']),
                  entryAnimation: EntryAnimation.TOP_RIGHT,
                ));
      },
      onLaunch: (Map<String, dynamic> message) async {
        message = fixMessageTitleAndBody(message);

        await showDialog(
            context: context,
            builder: (_) => NotificationDialog(
                  imageWidget: MottoLogoWidget(),
                  title: Text(
                    message['notification']['title'],
                    style:
                        TextStyle(fontSize: 22.0, fontWeight: FontWeight.w600),
                  ),
                  description: AutoSizeText(message['notification']['body']),
                  entryAnimation: EntryAnimation.TOP_RIGHT,
                ));
      },
    );
  }

  void pop() => Provider.of<NavigationService>(context, listen: false).goBack();

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    Constant.setScreenAwareConstant(context);
    return Stack(children: [
      //Fix background for all pages
      StripsWidget(
          color1: kielegat_orange,
          color2: kielegat_red,
          gap: 135,
          noOfStrips: 9),
      //Start building base widgets
      Scaffold(
        backgroundColor: Colors.transparent,
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(50),
          child: AppBar(
            leading: AppLeadingWidget(),
            title: AppHeader(),
            backgroundColor: kielegat_orange,
            bottomOpacity: 0.0,
            elevation: 0.0,
          ),
        ),
        body: Container(
            child: Navigator(
          key: Provider.of<NavigationService>(context, listen: false)
              .navigatorKey,
          observers: [MyNavigatorObserver(context)],
          onGenerateRoute: generateRoute,
          initialRoute: HomeRoute,
        )),
        bottomNavigationBar: BottomNav(),
      ),
    ]);
  }
}
