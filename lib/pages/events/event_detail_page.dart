import 'package:flutter/material.dart';
import 'package:kielegat_app/models/eventItem.dart';
import 'package:kielegat_app/widgets/events/event_detail_widget.dart';

class EventDetailPage extends StatelessWidget {
  final event;
  const EventDetailPage(this.event, {Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return EventDetail(event: event as EventItem);
  }
}
