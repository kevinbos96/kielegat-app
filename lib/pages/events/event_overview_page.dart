import 'package:auto_size_text/auto_size_text.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:kielegat_app/common/kielegat_style.dart';
import 'package:kielegat_app/locator.dart';
import 'package:kielegat_app/models/eventItem.dart';
import 'package:kielegat_app/providers/event_provider.dart';
import 'package:kielegat_app/widgets/events/event_item.dart';

class EventsOverview extends StatefulWidget {
  @override
  _EventsOverviewState createState() => _EventsOverviewState();
}

class _EventsOverviewState extends State<EventsOverview> {
  List<EventItem> eventList;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final eventProvider = locator<EventsProvider>();

    return Container(
      child: StreamBuilder(
          stream: eventProvider.fetchEventsAsStream(),
          builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasData) {
              eventList = snapshot.data.docs
                  .map((doc) => EventItem.fromMap(doc.data(), doc.id))
                  .toList();
              eventList = eventList
                  .where((e) => e.eventDate
                      .isAfter(DateTime.now().subtract(Duration(hours: 5))))
                  .toList();
              eventList.sort((a, b) => a.eventDate.compareTo(b.eventDate));
              return eventList.isNotEmpty
                  ? GridView.builder(
                      padding: kielegat_base_padding,
                      itemCount: eventList.length,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 1,
                          childAspectRatio: 10 / 6,
                          crossAxisSpacing: 10,
                          mainAxisSpacing: 10),
                      itemBuilder: (buildContext, index) =>
                          EventItemWidget(eventItem: eventList[index]),
                    )
                  : geenEvenementen();
            } else {
              return Text('Veel leut ophalen');
            }
          }),
    );
  }

  Widget geenEvenementen() {
    return Padding(
      padding: kielegat_base_padding,
      child: Container(
        padding: EdgeInsets.all(16.0),
        color: Colors.white,
        child: AutoSizeText.rich(
          TextSpan(
              text: "Er staan geen evenementen gepland",
              style: kielegat_head23),
          textAlign: TextAlign.left,
          style: kielegat_head20,
          minFontSize: 18,
          maxLines: 2,
        ),
      ),
    );
  }
}
