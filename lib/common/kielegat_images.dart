import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kielegat_app/common/url_launcher.dart';
import 'package:kielegat_app/providers/general_info_provider.dart';
import 'package:kielegat_app/routing/route_names.dart';
import 'package:kielegat_app/widgets/navigation/menu_item.dart';

import '../locator.dart';

final String polonaiseAppbar1 = 'images/svg/onderdelen_polonaise01.svg';
final Widget polonaiseAppbar1Svg =
    SvgPicture.asset(polonaiseAppbar1, semanticsLabel: 'polonaiseAppbar1');

//
final generalInfoProvider = locator<GeneralInfoProvider>();
final Image placeHolder = Image.asset("images/Placeholder.jpg");

List<MenuItem> menu_overlay_items = [
  MenuItem(
    menu_items[0],
    route: NewsRoute,
  ),
  MenuItem(
    menu_items[1],
    route: EventsRoute,
  ),
  MenuItem(
    menu_items[13],
    func: () {
      UrlLauncher().launchURL(generalInfoProvider.getjukeboxUrl());
    },
  ),
  MenuItem(
    menu_items[6],
    route: LeutpenningenRoute,
  ),
  MenuItem(
    menu_items[15],
    func: () {
      UrlLauncher().launchURL(generalInfoProvider.getSanseveriaUrl());
    },
  ),
  MenuItem(
    menu_items[16],
    func: () {
      UrlLauncher().launchURL(generalInfoProvider.getCarnavalsMenuUrl());
    },
  ),
  MenuItem(
    menu_items[17],
    route: Wandeling,
  ),
  MenuItem(
    menu_items[14],
    route: PrinsRoute,
  ),
  MenuItem(
    menu_items[3],
    func: () {
      UrlLauncher().launchURL(generalInfoProvider.getKieleshop());
    },
  ),
  MenuItem(
    menu_items[4],
    route: SponsorRoute,
  ),
];

List<SvgPicture> menu_items = [
  SvgPicture.asset(
    'images/svg/nieuws.svg',
  ),
  SvgPicture.asset(
    'images/svg/evenementen.svg',
  ),
  SvgPicture.asset(
    'images/svg/kiekeboek.svg',
  ),
  SvgPicture.asset(
    'images/svg/kieleshop.svg',
  ),
  SvgPicture.asset(
    'images/svg/sponsors.svg',
  ),
  SvgPicture.asset(
    'images/svg/motto.svg',
  ),
  SvgPicture.asset(
    'images/svg/leutpenningen_button.svg',
  ),
  SvgPicture.asset(
    'images/svg/contact.svg',
  ),
  SvgPicture.asset(
    'images/svg/taxi_button.svg',
  ),
  SvgPicture.asset(
    'images/svg/optocht.svg',
  ),
  SvgPicture.asset(
    'images/svg/brakkensliert.svg',
  ),
  SvgPicture.asset(
    'images/svg/bel_een_taxi.svg',
  ),
  SvgPicture.asset(
    'images/svg/intocht.svg',
  ),
  SvgPicture.asset(
    'images/svg/jukebox.svg',
  ),
  SvgPicture.asset(
    'images/svg/prins.svg',
  ),
  SvgPicture.asset(
    'images/svg/sanseveria-tv.svg',
  ),
  SvgPicture.asset(
    'images/svg/dieliver-naor-oe.svg',
  ),
  SvgPicture.asset(
    'images/svg/kielegatse-kilometer.svg',
  ),
];
