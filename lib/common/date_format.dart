

  String mothToText(int month) {
    switch (month) {
      case 1:
        return "JAN";
      case 2:
        return "FEB";
      case 3:
        return "MRT";
      case 4:
        return "APR";
      case 5:
        return "MEI";
      case 6:
        return "JUN";
      case 7:
        return "JUL";
      case 8:
        return "AUG";
      case 9:
        return "SEPT";
      case 10:
        return "OKT";
      case 11:
        return "NOV";
      case 12:
        return "DEC";
    }
    return "";
  }

  String dateToString(DateTime dt){
    return "${dt.day}-${dt.month}-${dt.year}";
  }
