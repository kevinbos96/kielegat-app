import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

TextStyle eventNameStyle = TextStyle(color: Colors.black87, fontSize: 35.0, fontWeight: FontWeight.w900);
TextStyle dateStyle = TextStyle(color: Colors.white, fontSize: 40.0, fontWeight: FontWeight.w900);
TextStyle dateSmallStyle = TextStyle(color: Colors.white70, fontSize: 25.0, fontWeight: FontWeight.w600);

Color dateBackground = Color.fromARGB(1, 239, 129, 49);
