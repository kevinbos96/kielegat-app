  
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

const  kielegat_red = Color(0xffe3001b);
const  kielegat_orange = Color(0xffec6707);
const  kielegat_blue = Color(0xff004493);
const  kielegat_black = Color(0xff323232);
const  kielegat_yellow = Color(0xffffc416);

EdgeInsets kielegat_base_padding = EdgeInsets.all(16);
const kielegat_background_white = Colors.white;


TextStyle kielegat_head16 = TextStyle(fontSize: ScreenUtil().setSp(16), color: kielegat_red, fontFamily: "Titan One");
TextStyle kielegat_head15 = TextStyle(fontSize: ScreenUtil().setSp(16), color: kielegat_orange, fontFamily: "Titan One");


TextStyle kielegat_head20 = TextStyle(fontSize: ScreenUtil().setSp(20), color: kielegat_red, fontFamily: "Titan One");
TextStyle kielegat_head23 = TextStyle(fontSize: ScreenUtil().setSp(23), color: kielegat_red, fontFamily: "Titan One");

TextStyle kielegat_head30 = TextStyle(fontSize: ScreenUtil().setSp(30), color: kielegat_red, fontFamily: "Titan One");

TextStyle kielegat_small12 = TextStyle(fontSize: ScreenUtil().setSp(12));

TextStyle kielegat_text_style = TextStyle( fontSize: ScreenUtil().setSp(15), color: kielegat_black, fontFamily: "Lato");
TextStyle kielegat_text_20_style = TextStyle( fontSize: ScreenUtil().setSp(20), color: kielegat_black, fontFamily: "Lato");