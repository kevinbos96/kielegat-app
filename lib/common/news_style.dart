import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

TextStyle newsTitle = TextStyle(color: Colors.white70, fontSize: ScreenUtil().setSp(20, ), fontWeight: FontWeight.w600);

//Set a way to big fontsize, AutoSizeText will make it event and make it fit!
//Set so high so it is always max font size that will fit
TextStyle newsDateDay = TextStyle(fontSize: ScreenUtil().setSp(30,), color: Colors.white, fontWeight: FontWeight.w300, fontFamily: "Titan One");
TextStyle newsDateMonth = TextStyle(fontSize: ScreenUtil().setSp(30.0), color: Colors.white,  fontWeight: FontWeight.w300, fontFamily: "Titan One");
TextStyle newsDateYear = TextStyle(fontSize: ScreenUtil().setSp(30.0), color: Colors.white, fontWeight: FontWeight.w300, fontFamily: "Titan One");
