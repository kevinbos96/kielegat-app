import 'dart:collection';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class NavigationService with ChangeNotifier {
  String _currentRoute = "home";
  String goBackRoute = "";
  String get currentRoute => _currentRoute;
  Queue<String> history = Queue.from(["home"]);

  bool _showLeading = false;
  bool get showLeading => _showLeading;
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  //Future<dynamic>
  navigateTo(String routeName, {var content}) {
    bool notHome = !checkIsHome(currentRoute);
    if (setSelected(routeName)) {
      navigatorKey.currentState.popAndPushNamed(routeName, arguments: content);

      notifyListeners();
    }
  }

  bool goBack() {
    String newR = this.history.last;
    if (!checkIsHome(currentRoute)) {
      _showLeading = history.last != "home";
      this._currentRoute = history.removeLast();
      navigatorKey.currentState.popAndPushNamed(newR);
      notifyListeners();
      return true;
    } else {
      SystemChannels.platform.invokeMethod('SystemNavigator.pop');
    }
    _showLeading = currentRoute != "home";
    notifyListeners();
    return false;
  }

  goBackCheckInterceptor(String oldR, String newR) {
    if (checkIsHome(currentRoute)) {
      SystemChannels.platform.invokeMethod('SystemNavigator.pop');
    }
    if (history.last != oldR) {
      navigateTo(history.removeLast());
    }
  }

  bool isSelected(String item) {
    return currentRoute == item || currentRoute.contains(item);
  }

  bool setSelected(String newRoute) {
    print("setSelected before if: " + newRoute);
    if (currentRoute != newRoute) {
      print("setSelected after if: " + newRoute);
      print("setSelected after if current: " + currentRoute);
      if (!currentRoute.toLowerCase().contains("detail"))
        history.add(currentRoute);
      _showLeading = newRoute != "home";
      this._currentRoute = newRoute;
      notifyListeners();
      return true;
    } else {
      return false;
    }
  }

  bool checkIsHome(String route) {
    return route == "home" || route == "/" || route == "";
  }
}
