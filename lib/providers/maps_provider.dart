import 'package:flutter/material.dart';
import 'package:kielegat_app/api/firebase/firebase_api.dart';
import 'package:kielegat_app/models/map_item.dart';
import 'package:kielegat_app/models/mapmarker.dart';

class MapsProvider extends ChangeNotifier {
  Api _api = Api('locations');

  List<MapMarker> markers;
  List<MapItem> polyline;

  Future<MapItem> fetchMapItem(String page) async {
    var result = await _api.streamMultibleFilteredDataCollection(
        "page_id", page, "type", 2);
    polyline =
        result.docs.map((doc) => MapItem.fromMap(doc.data(), doc.id)).toList();
    return polyline.first;
  }
}
