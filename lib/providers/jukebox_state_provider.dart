import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';

class JukeboxStateProvider  extends ChangeNotifier{
  String _id;
  AudioPlayerState _playerState;

  String get id {
    return _id;
  }

  AudioPlayerState get playerState{
    return _playerState;
  }


  SetState(String id){
    _id = id;
    // _playerState = playerState;
    notifyListeners();
  }



}