import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:kielegat_app/api/firebase/firebase_api.dart';
import 'package:kielegat_app/models/sponsor_group.dart';

class SponsorProvider extends ChangeNotifier {
  Api _api = Api('Sponsoren');

  List<SponsorGroup> sponsorGroups;

  Future<List<SponsorGroup>> fetchSponsors() async {
    var result = await _api.getDataCollection();
    sponsorGroups = result.docs
        .map((doc) => SponsorGroup.fromMap(doc.data(), doc.id))
        .toList();
    return sponsorGroups;
  }

  Stream<QuerySnapshot> fetchSponsorsAsStream() {
    return _api.streamOrdedDataCollection("type");
  }

  Future<SponsorGroup> getSponsorById(String id) async {
    var doc = await _api.getDocumentById(id);
    return SponsorGroup.fromMap(doc.data(), doc.id);
  }
}
