import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:kielegat_app/api/firebase/firebase_api.dart';

class JukeboxProvider extends ChangeNotifier {
  Api _api = Api("Jukebox");

   Stream<QuerySnapshot> fetchJukeBox() {
    return _api.streamDataCollection();

  }

  

}
