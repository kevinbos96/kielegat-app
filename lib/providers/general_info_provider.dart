import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/widgets.dart';
import 'package:kielegat_app/api/firebase/firebase_api.dart';
import 'package:kielegat_app/models/customPage.dart';

class GeneralInfoProvider extends ChangeNotifier {
  Api _api = Api('page');

  List<CustomPage> articles;
  String phone;
  String shop;
  String sanseveria;
  String jukebox;
  String CarnavalsMenu;

  Future<List<CustomPage>> fetchPage() async {
    var result = await _api.getDataCollection();
    articles = result.docs
        .map((doc) => CustomPage.fromMap(doc.data(), doc.id))
        .toList();
    return articles;
  }

  Stream<QuerySnapshot> fetchPageAsStream() {
    return _api.streamDataCollection();
  }

  Future<CustomPage> getPageById(String id) async {
    var doc = await _api.getDocumentById(id);
    return CustomPage.fromMap(doc.data(), doc.id);
  }

  Future removePage(String id) async {
    await _api.removeDocument(id);
    return;
  }

  void initialize() {
    getPhoneNumber();
    getKieleshop();
    getSanseveriaUrl();
    getjukeboxUrl();
    getCarnavalsMenuUrl();
  }

  String getPhoneNumber() {
    if (phone == null) {
      getPageById('taxi').then((p) => phone = p.tel);
      notifyListeners();
    }
    return phone;
  }

  String getKieleshop() {
    if (shop == null || shop == '') {
      getPageById('kieleshop').then((p) => {shop = p.url, notifyListeners()});
    }
    return shop;
  }

  String getSanseveriaUrl() {
    if (sanseveria == null || sanseveria == '') {
      getPageById('sanseveria')
          .then((p) => {sanseveria = p.url, notifyListeners()});
    }
    return sanseveria;
  }

  String getjukeboxUrl() {
    if (jukebox == null || jukebox == '') {
      getPageById('jukebox').then((p) => {jukebox = p.url, notifyListeners()});
    }
    return jukebox;
  }

  String getCarnavalsMenuUrl() {
    if (CarnavalsMenu == null || CarnavalsMenu == '') {
      getPageById('CarnavalsMenu')
          .then((p) => {CarnavalsMenu = p.url, notifyListeners()});
    }
    return CarnavalsMenu;
  }
}
