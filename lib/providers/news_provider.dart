import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:kielegat_app/api/firebase/firebase_api.dart';
import 'package:kielegat_app/models/article.dart';

class NewsProvider extends ChangeNotifier {
  Api _api = Api('news');

  List<Article> articles;

  Future<List<Article>> fetchArticles() async {
    var result = await _api.getDataCollection();
    articles =
        result.docs.map((doc) => Article.fromMap(doc.data(), doc.id)).toList();
    return articles;
  }

  Stream<QuerySnapshot> fetchArticlesAsStream() {
    return _api.streamDataCollection();
  }

  Future<Article> getArticleById(String id) async {
    var doc = await _api.getDocumentById(id);
    return Article.fromMap(doc.data(), doc.id);
  }

  Future removeArticle(String id) async {
    await _api.removeDocument(id);
    return;
  }
}
