import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:kielegat_app/api/firebase/firebase_api.dart';
import 'package:kielegat_app/models/eventItem.dart';

class EventsProvider extends ChangeNotifier {
  Api _api = Api('events');

  List<EventItem> articles;

  Future<List<EventItem>> fetchEvents() async {
    var result = await _api.getDataCollection();
    articles = result.docs
        .map((doc) => EventItem.fromMap(doc.data(), doc.id))
        .toList();
    return articles;
  }

  Stream<QuerySnapshot> fetchEventsAsStream() {
    return _api.streamDataCollection();
  }

  Future<EventItem> getEventItemById(String id) async {
    var doc = await _api.getDocumentById(id);
    return EventItem.fromMap(doc.data(), doc.id);
  }

  Future removeEventItem(String id) async {
    await _api.removeDocument(id);
    return;
  }
}
