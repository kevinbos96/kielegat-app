import 'package:flutter/material.dart';
import 'package:kielegat_app/routing/router.dart';
import 'package:provider/provider.dart';

import 'navigation_service.dart';

class MyNavigatorObserver extends NavigatorObserver {
  final BuildContext context;

  MyNavigatorObserver(this.context);

  void didPop(Route<dynamic> oldRoute, Route<dynamic> newRoute) {
    if (newRoute != null) {
      NavigationService service =
          Provider.of<NavigationService>(context, listen: false);
      var old = oldRoute as FadeRoute;
      var newR= newRoute as FadeRoute;
      service.goBackCheckInterceptor(old.routeName, newR.routeName);
    }
  }
}
