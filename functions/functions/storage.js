const request = require('request')
const admin = require('firebase-admin');


const uploadNewFileFromStream = (bucketName) => (id) => (readStream) => {
   
    const fileName = `news/${id}.jpg`;
    const bucket = admin.storage().bucket(bucketName);
    const file = bucket.file(`${fileName}`);

    const writeStream = file.createWriteStream({
        metadata: {
            contentType: 'image/jpeg',
        }
    });
    const result = new Promise((resolve, reject) => {
        writeStream.on('error', function (err) {
            reject(err);
        });
        writeStream.on('finish', function () {
            const publicUrl = `https://firebasestorage.googleapis.com/v0/b/${bucket.name}/o/${(encodeURI(fileName)).replace("\/", "%2F")}?alt=media`;
            
            resolve(publicUrl);
        });
    });
    readStream.pipe(writeStream);

    return result;
};


const getStreamFromUrl = (url) => {
    return request(url);
};


module.exports = {

        uploadNewFile: (bucketName, id, stream) => uploadNewFileFromStream(bucketName)(id)(stream),
        uploadNewSpotPictureFromUrl: (bucketName, id, url) => uploadNewFileFromStream(bucketName)(id)(getStreamFromUrl(url)),
};
