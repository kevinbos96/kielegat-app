const functions = require('firebase-functions');
const path = require('path');
const os = require('os');

const Fetch = require('./fetch');
const Database = require('./database')

const HOST = "http://www.carnavalbreda.nl/wp-json"
const f = new Fetch(HOST)
const d = new Database()


async function update() {
    try {
        news = await f.getNewsItems();
        let result = await d.insertAllNewNewsItems(news)
        console.log(result)
        return result

    } catch (err) {
        console.log(err)
        throw err;
    }
}


exports.updateDatabase = functions.https.onRequest(async (req, res) => {

    try {

        let result = await update()

        let obj = {
            "items": result,
            "succes": true
        }

        res.json(obj)
    } catch (error) {

        let obj = {
            "error": error,
            "succes": false
        }
        res.json(obj);
    };
});