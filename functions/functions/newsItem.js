const Entities = require('html-entities').XmlEntities;
const entities = new Entities();

const DESCRIPTION_LENGTH = 150;

class NewsItem {
    constructor(wpId, title, date, modified, content, imageUrl) {
        this.wpId = wpId;
        this.title = cleanContent(title);
        this.date = date;
        this.modified = modified;
        this.content = cleanContent(content);
        this.description = prepareDescription(content);
        this.imageUrl = imageUrl
    }
}


function cleanContent(content) {

    //optional cleaning table style tags
    //todo remove video tags and return an direct link 
    //set html entity tags to entity
    str = entities.decode(content);
    //remove left over nbsp (space)
    str = replaceAll(str, "&nbsp;", " ");
    //remove left over line breaks
    str = replaceAll(str, "\n", "");
    return str;

}

function prepareDescription(content) {
    //remove html tags
    str = content.replace(/(<([^>]+)>)/ig, "");
    str = replaceAll(str, "&nbsp;", " ")
    str = replaceAll(str, "\n", "")
    //cut content with set length
    return str.substring(0, DESCRIPTION_LENGTH)
}

function replaceAll(str, find, replace) {
    return str.replace(new RegExp(find, 'g'), replace);
}

module.exports = NewsItem;


