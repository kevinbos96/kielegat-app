var WPAPI = require('wpapi'); //https://github.com/WP-API/node-wpapi
const NewsItem = require('./newsItem');



class Fetch {

    constructor(HOST) {
        this.HOST = HOST
        this._apiPromise = WPAPI.discover(HOST);
        this._placeHolder = "https://firebasestorage.googleapis.com/v0/b/kielegat-d87c5.appspot.com/o/FCMImages%2FPlaceholder.jpg?alt=media&token=95dc5312-d4f1-4386-8be3-4fd744337a6c"
    }

    async getNewsItems() {
        var news = []
        try {
            // console.log(this._apiPromise)/
            const site = await this._apiPromise

            const posts = await site.posts()
            for (var i = 0; i < posts.length; i++) {
                //define current item as new array and fill it with data

                //Fetch image url
                const imageUrl =await this.getImage(site, posts[i]);

                var art = new NewsItem(posts[i]["id"], posts[i]["title"]["rendered"], posts[i]["date"], posts[i]["modified_gmt"], posts[i]["content"]["rendered"], imageUrl)//
                news[i] = art;
            }
            return news;

        } catch (err) {
            throw err;
        }
    }

    async getImage(site, post) {
        try {
            // console.log(post);
            const media = await site.media().id(post.featured_media);            //post.featured_media
            return media.source_url;
        } catch (err) {
            console.log(err)
            console.log("media id: " + post.featured_media)
            return this._placeHolder
        }

    }
}

module.exports = Fetch;


