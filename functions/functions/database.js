const admin = require('firebase-admin');
const functions = require('firebase-functions');
const Storage = require('./storage')
serviceAccount = require('./serviceAccount.json');




class Database {

    constructor() {
        const adminConfig = JSON.parse(process.env.FIREBASE_CONFIG);
        adminConfig.credential = admin.credential.cert(serviceAccount);
        admin.initializeApp(adminConfig);

        this.db = admin.firestore();
    }

    async insertNewsItem(newsItem) {
        try {
            let aNewItemRef = this.db.collection('news').doc("wpId" + newsItem.wpId);
            let setNewsItem = await aNewItemRef.set({
                wpId: newsItem.wpId,
                title: newsItem.title,
                date: newsItem.date,
                modified: admin.firestore.Timestamp.fromDate(new Date(newsItem.modified)),
                content: newsItem.content,
                description: newsItem.description,
                imageUrl: newsItem.imageUrl,
            });
            return setNewsItem;
        } catch (error) {
            return error
        }
    }

    async insertAllNewNewsItems(newsItems) {
        let responseJson = []
        let newsItemRef = this.db.collection('news')
        for (var i = 0; i < newsItems.length; i++) {
            try {
                let doc = await newsItemRef.doc("wpId" + newsItems[i].wpId).get()
                if (doc.exists) {
                    let currentItem = doc.data()
                    if (currentItem.modified != undefined) {
                        //cast to date
                        let currentModified = currentItem.modified._seconds;
                        let newModified = (new Date(newsItems[i].modified).getTime() / 1000);

                        if (currentModified < newModified) {
                            //Time to update!
                            let insertResponse = await this.insertItem(newsItems[i]);
                            responseJson.push(insertResponse);
                        }
                    }
                } else {
                    let insertResponse = await this.insertItem(newsItems[i]);
                    responseJson.push(insertResponse);
                }

            } catch (error) {
                console.log('Error getting document', error);
            }
        }
        return responseJson;
    }

    async insertItem(newsItem) {
        try {
            newsItem.imageUrl = await Storage.uploadNewSpotPictureFromUrl("kielegat-d87c5.appspot.com", newsItem.wpId, newsItem.imageUrl)
            await this.insertNewsItem(newsItem);
            return "Inserted" + newsItem.title;
        } catch (error) {
            console.log(error);
            return "Error inserting";
        }

    }

}

module.exports = Database;